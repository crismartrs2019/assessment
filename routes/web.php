<?php

use Illuminate\Support\Facades\Route;
use App\Models\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {

    return view("clientCreate");

});

Route::get("/clients","App\Http\Controllers\Client@list");


Route::get("/clients/create","App\Http\Controllers\Client@create");


//Route::get("/test",Client::class);
