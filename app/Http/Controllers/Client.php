<?php 

    namespace App\Http\Controllers;

    use App\Models\Client as ClientSql;
    use Illuminate\Http\Request;
    use Illuminate\Http\Response;
    use Illuminate\Routing\Controller;


    
    class Client extends Controller{


        public function create(Request $request,Response $response){

            $client = new ClientSql;

            $client -> phone = $request->input("phone");

            $client -> birth_date = $request->input("birth_date");

            $result = $client->save();

            return view("result",["status"=>($result==true)?"saved":"not saved"]);

        }


        public function list(Request $request,Response $response):Response{

            $clients = ClientSql::get();

            //# im gonna make a not effient method but is just for test, i should fliter the data and set a data type for output
            // i may use a template function 
            
            $result = "";

            for($i=0;$i<count($clients);$i++){

                $result .= "client: {id:".$clients[$i]["ID_CLIENT"].",phone:".$clients[$i]["PHONE"]."}<br>";

            }   

            return response($result);

        }


    }

?>